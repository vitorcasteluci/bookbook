import React from "react";
import { withStyles } from "@material-ui/core/styles";

import { Paper, CardMedia, Container, Button } from "@material-ui/core";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import loadBooksList from "../helpers/loadBooksList";
import { addBooks } from "../../action";
import removeButton from "../buttons/removeButton";
import moveToReadButton from "../buttons/moveToReadButton";

const styles = () => ({
  item: {
    margin: "0 auto",
    width: "100%",
    minWidth: "290px",
    maxWidth: "290px",
  },
  superior: {
    width: "100%",
    height: "4px",
    backgroundColor: "rgba(18,29,77,1)",
    borderRadius: "5px",
  },
  name: {
    color: "#3f51b5",
    fontFamily: "Monda",
    textTransform: "uppercase",
    fontSize: "16px",
    marginTop: "-12px",
    marginBottom: "0px",
  },
  author: {
    color: "#374242",
    fontFamily: "Monda",
    fontSize: "15px",
    marginTop: "0px",
    marginBottom: "10px",
  },
  cards: {},
  title: {
    textTransform: "uppercase",
    fontSize: "12px",
  },
  h2: {
    color: "#374242",
    fontFamily: "Monda",
    fontSize: "12px",
  },
  h3: {
    width: "100%",
    minWidth: "290px",
    maxWidth: "290px",
    margin: "0 auto",
    background:
      "linear-gradient(180deg, rgba(18,29,77,1) 0%, rgba(0,0,0,1) 100%)",
    borderRadius: "4px",
    color: "white",
    fontFamily: "Monda",
    fontSize: "18px",
  },
  media: {
    margin: "0 auto",
    marginBottom: "10px",
  },
  button: {
    backgroundColor: "#antiquewhite",
    color: "darkslateblue",
    marginBottom: "16px",
    marginTop: "16px",
    marginLeft: "4px",
    marginRight: "4px",
    borderColor: "darkslateblue",
  },
  button2: {
    backgroundColor: "white",
    color: "crimson",
    marginBottom: "16px",
    marginTop: "16px",
    marginLeft: "4px",
    marginRight: "4px",
    borderColor: "indianred",
  },
  header1: {
    color: "#406868",
    fontFamily: "Monda",
    fontSize: "16px",
  },
  buttonRoot: {
    flexGrow: 1,
  },
  welcome: {
    fontFamily: "Monda",
    fontSize: "14px",
    color: "gray",
  },
});

class ReadingBooks extends React.Component {
  finished = (bookId) => {
    moveToReadButton(this.props.user.id, bookId, this.props.currentToken).then(
      () =>
        loadBooksList(this.props.user.id, this.props.currentToken)
          .then((res) => res.json())
          .then((res) => {
            this.props.addBooks(res);
          })
    );
  };

  removeBook = (bookId) => {
    removeButton(this.props.user.id, bookId, this.props.currentToken).then(() =>
      loadBooksList(this.props.user.id, this.props.currentToken)
        .then((res) => res.json())
        .then((res) => {
          this.props.addBooks(res);
        })
    );
  };

  render() {
    const { classes } = this.props;
    if (this.props.books.length > 0) {
      return (
        <>
          <h1 className={classes.h3}>Em leitura</h1>
          {this.props.books.map((book, key) => (
            <>
              <div className={classes.cards}>
                <Paper elevation={5} className={classes.item} key={key}>
                  <Paper elevation={25}>
                    <div className={classes.superior}></div>
                    <br />
                    <h1 className={classes.name}>{book.title}</h1>
                    <h2 className={classes.author}>{book.author}</h2>

                    <Container>
                      <hr />
                      <CardMedia
                        className={classes.media}
                        image={book.image_url}
                        title={book.title}
                        style={{ height: 225, width: 150 }}
                      />
                      <hr />
                    </Container>
                  </Paper>
                  <Paper>
                    <Button
                      className={classes.button}
                      size="small"
                      variant="outlined"
                      color="primary"
                      onClick={() => this.finished(book.id)}
                    >
                      Concluído
                    </Button>
                    <Button
                      className={classes.button2}
                      size="small"
                      variant="outlined"
                      color="secondary"
                      onClick={() => this.removeBook(book.id)}
                    >
                      Remover
                    </Button>
                  </Paper>
                </Paper>
                <br />
                <br />
              </div>
            </>
          ))}
        </>
      );
    } else {
      return (
        <>
          <h1 className={classes.h3}>Em leitura</h1>
          <>
            <div className={classes.cards}>
              <Paper elevation={20} className={classes.item}>
                <Paper elevation={25}>
                  <br />
                  <Container>
                    <p className={classes.welcome}>●</p>
                    <Container>
                      <p className={classes.welcome}>
                        Adicione livros a sua lista de leituras recentes
                      </p>
                    </Container>
                    <p className={classes.welcome}>●</p>
                  </Container>
                  <br />
                </Paper>
              </Paper>
              <br />
              <br />
            </div>
          </>
        </>
      );
    }
  }
}

const mapStateToProps = (state) => ({
  currentToken: state.session.currentToken,
  user: state.session.user,
  books: state.bookshelf.books.filter((book) => book.shelf === 2),
});

const mapDispatchToProps = (dispatch) => ({
  addBooks: (books) => dispatch(addBooks(books)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(withStyles(styles)(ReadingBooks)));

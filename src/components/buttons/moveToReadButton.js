const moveToReadButton = (userId, bookId, token) => {
  return fetch(
    `https://ka-users-api.herokuapp.com/users/${userId}/books/${bookId}`,
    {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: token,
      },
      body: JSON.stringify({
        book: {
          shelf: 3,
        },
      }),
    }
  )
}
export default moveToReadButton;
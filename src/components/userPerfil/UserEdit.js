import React from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import TextField from '@material-ui/core/TextField';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

import { updateUser } from '../../action';

const useStyles = (theme) => ({
  h3: {
    margin: '0 auto',
    background:
      'linear-gradient(180deg, rgba(66,54,32,1) 0%, rgba(0,0,0,1) 100%)',
    borderRadius: '4px',
    color: 'white',
    width: '100%',
    fontFamily: 'Monda',
    fontSize: '18px',
  },
  button: {
    marginLeft: '4px',
    marginRight: '4px',
  },
});

class UserEdit extends React.Component {
  state = {
    name: { value: '', error: '', helperText: '' },
    image: { value: '' },
    about: { value: '', error: '', helperText: '' },
  };

  updateUser = () => {
    console.log(this.state);
    fetch(`https://ka-users-api.herokuapp.com/users/${this.props.user.id}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: this.props.currentToken,
      },
      body: JSON.stringify({
        user: {
          name: this.state.name.value,
          image_url: this.validationImage(),
          about: this.state.about.value,
        },
      }),
    })
      .then((res) => res.json())
      .then((res) => {
        this.props.updateUser(res);
        localStorage.setItem('currentUser', JSON.stringify(this.props.user));
        this.props.cancelEditing();
      });
  };
  componentDidMount() {
    this.setState({
      name: { ...this.state.name, value: this.props.user.name },
      image: { ...this.state.image, value: this.props.user.image_url },
      about: { ...this.state.about, value: this.props.user.about },
    });
  }
  setValueName = (e) => {
    const value = e.target.value;
    this.setState({
      ...this.state,
      name: { ...this.state.name, value: value },
    });
  };
  setValueImage = (e) => {
    const value = e.target.value;
    this.setState({
      ...this.state,
      image: { ...this.state.image, value: value },
    });
  };
  setValueAbout = (e) => {
    const value = e.target.value;
    this.setState({
      ...this.state,
      about: { ...this.state.about, value: value },
    });
  };

  validationName = () => {
    const name = this.state.name.value;
    if (name === '') {
      return 'Ops! Não esqueça de preencher este campo';
    } else if (name.length < 7) {
      return 'O nome não deve possuir menos que 7 caracteres';
    } else if (name.length > 56) {
      return 'O nome não deve possuir mais que 56 caracteres';
    } else {
      return '';
    }
  };

  validationAbout = () => {
    const about = this.state.about.value;
    if (about.length > 150) {
      return 'Este campo não deve possuir mais que 150 caracteres';
    } else {
      return '';
    }
  };

  validationImage = () => {
    if (this.state.image.value.length === 0) {
      return 'http://www.ecp.org.br/wp-content/uploads/2017/12/default-avatar.png';
    }
    return this.state.image.value;
  };

  submeter = () => {
    const nameError = this.validationName();
    const aboutError = this.validationAbout();
    this.setState({
      ...this.state,

      name: {
        value: this.state.name.value,
        error: nameError === '' ? '' : 'true',
        helperText: nameError,
      },
      about: {
        value: this.state.about.value,
        error: aboutError === '' ? '' : 'true',
        helperText: aboutError,
      },
    });
    if (nameError === '' && aboutError === '') {
      this.updateUser();
    }
  };

  render() {
    const { classes } = this.props;
    if (this.props.isEditing === true) {
      return (
        <form>
          <br />
          <br />
          <TextField
            variant="outlined"
            size="small"
            label="Nome:"
            value={this.state.name.value}
            onChange={this.setValueName}
            helperText={this.state.name.helperText}
            error={this.state.name.error}
          />
          <br />
          <br />
          <TextField
            multiline
            variant="outlined"
            size="small"
            label="Imagem de perfil (URL):"
            rows={3}
            value={this.state.image.value}
            onChange={this.setValueImage}
          />
          <br />
          <br />
          <TextField
            multiline
            variant="outlined"
            size="small"
            rows={3}
            label="Sobre você:"
            value={this.state.about.value}
            onChange={this.setValueAbout}
            helperText={this.state.about.helperText}
            error={this.state.about.error}
          />
          <br />
          <br />
          <Button
            className={classes.button}
            size="small"
            variant="contained"
            color="primary"
            onClick={this.submeter}
          >
            Salvar
          </Button>

          <Button
            className={classes.button}
            size="small"
            variant="contained"
            color="secondary"
            onClick={this.props.cancelEditing}
          >
            Cancelar
          </Button>
        </form>
      );
    }
    return null;
  }
}

const mapStateToProps = (state) => ({
  currentToken: state.session.currentToken,
  user: state.session.user,
});

const mapDispatchToProps = (dispatch) => ({
  updateUser: (newUser) => dispatch(updateUser(newUser)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(withStyles(useStyles)(UserEdit)));

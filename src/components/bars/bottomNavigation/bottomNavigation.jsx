import React from 'react';
import { Link } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import PersonIcon from '@material-ui/icons/Person';
import SearchIcon from '@material-ui/icons/Search';
import PublicIcon from '@material-ui/icons/Public';
import SettingsIcon from '@material-ui/icons/Settings';
const useStyles = makeStyles({});
export default function SimpleBottomNavigation() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  return (
    <BottomNavigation
      value={value}
      onChange={(event, newValue) => {
        setValue(newValue);
      }}
      showLabels
      className={classes.root}
    >
      <BottomNavigationAction label="Explorar" icon={<PublicIcon />} />
      <Link to="/profile">
        <BottomNavigationAction
          label="Perfil"
          showLabel="true"
          icon={<PersonIcon />}
        />
      </Link>
      <BottomNavigationAction label="Buscar" icon={<SearchIcon />} />
      <BottomNavigationAction label="Configurações" icon={<SettingsIcon />} />
    </BottomNavigation>
  );
}

import React from "react";
import "../signUp/signUp.css";
import Background from "./background.jpg";
import RafaelPenteado from "./rafaelpenteado.png";
import vitorCastelucci from "./vitorcastelucci.png";
import LuizMileck from "./luizmileck.png";
import IsaPriviate from "./isapriviate.png";
import RafaelScherer from "./rafaelcherer.png";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import { withStyles } from "@material-ui/core/styles";
import { Container, InputLabel } from "@material-ui/core";
import "./aboutUs.css";

const useStyles = (theme) => ({
  photo: {
    borderRadius: "50%",
  },
  name: {
    color: "#3f51b5",
    fontFamily: "Monda",
    textTransform: "uppercase",
    fontSize: "16px",
    marginTop: "3px",
    marginBottom: "5px",
  },
});

class AboutUs extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <>
        <Grid item xs={12} sm={6}>
          <Paper className={classes.paper}>
            <img
              className="background"
              id="backgroundDefault "
              src={Background}
              alt="background"
              width="1225"
            />
          </Paper>
        </Grid>
        <Grid item xs={12} sm={6}>
          <Paper className={classes.paper}>
            <div className="background">
              <div className="side">
                <div id="bookbook">BookBook</div>
                <div id="bookbook2"> Desenvolvedores</div>
              </div>

              <br />

              <div className="side2">
                <div id="signup">
                  <div id="title"> </div>
                  <br />
                  <Container maxWidth="sm" className="box">
                    <InputLabel>Equipe de desenvolvedores ReactJS</InputLabel>
                    <br />
                    <br />
                    <Container>
                      <img
                        className={classes.photo}
                        src={RafaelPenteado}
                        alt="Rafael Penteado"
                        width="110"
                        height="100"
                      />
                      <Container>
                        <h1 className={classes.name}>Rafael Penteado </h1>
                      </Container>
                    </Container>
                    <Container>
                      {" "}
                      <img
                        className={classes.photo}
                        src={IsaPriviate}
                        alt="Isabela Priviate"
                        width="110"
                        height="100"
                      />
                      <Container>
                        <h1 className={classes.name}>Isabela Priviate</h1>
                      </Container>
                    </Container>
                    <Container>
                      {" "}
                      <img
                        className={classes.photo}
                        src={vitorCastelucci}
                        alt="Vitor Castelucci"
                        width="110"
                        height="100"
                      />
                      <Container>
                        <h1 className={classes.name}>Vitor Casteluci</h1>
                      </Container>
                    </Container>
                    <Container>
                      {" "}
                      <img
                        className={classes.photo}
                        src={LuizMileck}
                        alt="Luiz Henrique Mileck"
                        width="110"
                        height="100"
                      />
                      <Container>
                        <h1 className={classes.name}>
                          Luiz Henrique Sydney Mileck
                        </h1>
                      </Container>
                    </Container>
                    <Container>
                      {" "}
                      <img
                        className={classes.photo}
                        src={RafaelScherer}
                        alt="Rafael Scherer"
                        width="110"
                        height="100"
                      />
                      <Container>
                        <h1 className={classes.name}>Rafael Delgado Scherer</h1>
                      </Container>
                    </Container>
                  </Container>
                </div>
              </div>
            </div>
          </Paper>
        </Grid>
      </>
    );
  }
}

export default withStyles(useStyles)(AboutUs);

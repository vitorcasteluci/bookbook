const defaultState = { currentToken: '', user: [] };

const login = (state = defaultState, action) => {
  switch (action.type) {
    case 'LOGIN':
      const { currentToken } = action;
      const { user } = action;
      return { ...state, currentToken, user };
    case 'UPDATE_USER':
      const { newUser } = action;
      return { ...state, user: newUser };
    case 'CLEAR_LOGIN':
      return { ...state, currentToken: '', user: [] };
    default:
      return state;
  }
};

export default login;
